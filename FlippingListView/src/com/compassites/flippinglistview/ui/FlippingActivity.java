package com.compassites.flippinglistview.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.compassites.flippinglistview.R;
import com.compassites.flippinglistview.data.ListInfo;
import com.compassites.flippinglistview.data.ListInfo.ListData;
import com.compassites.flippinglistview.db.FlippingDbAdapter;
import com.compassites.flippinglistview.utility.Rotate3dAnimation;

public class FlippingActivity extends Activity implements LoaderCallbacks<List<ListData>>{
	static String TAG = FlippingActivity.class.getSimpleName();
	
	Context aContext = null;
    ListView mListView = null;
    private MyArrayAdapter mParseAdapter;
    ArrayList<ListData> mCityList; // list of data  to be added
    ArrayList<ListData> mtempList;
    ArrayList<Integer> mCitiId;
    private int pos = 0;
    private int visibleChildCount;
    private static boolean dbFlag = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_flipping);
		
		aContext = FlippingActivity.this; 
		mCityList = new ArrayList<ListData>();
        mtempList = new ArrayList<ListData>();
        mCitiId = new ArrayList<Integer>();
        mListView = (ListView)findViewById(android.R.id.list);
        mParseAdapter = new MyArrayAdapter(aContext, null);
        mListView.setAdapter(mParseAdapter);
        getLoaderManager().initLoader(0, null, FlippingActivity.this);
        
        
        Button addBtn = (Button)findViewById(R.id.add_item);
        addBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mtempList != null && mtempList.size() != 0){
					ListData selectedCity = mtempList.get(0);
					mCityList.add(selectedCity);
					mtempList.remove(selectedCity);
					Log.d(TAG, "addBtn onClick(): mUserList.size() = "+mCityList.size()+" mtempList.size() = "+mtempList.size());
					mParseAdapter.setData(mCityList);
				}
				
			}
		});
        
        Button flipBtn = (Button)findViewById(R.id.flip_item);
        flipBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				visibleChildCount = (mListView.getLastVisiblePosition() - mListView.getFirstVisiblePosition()) + 1;
				
				Log.d(TAG,"#niraj visibleChildCount = "+visibleChildCount+" pos = "+pos);
				startAnimation(pos);
			}
		});
	}
	
	
	private void startAnimation(int position) {
		
		Log.d(TAG,"#niraj visibleChildCount = "+visibleChildCount+" position = "+position);
		
		if(position >= visibleChildCount)
			return;

		View view = mListView.getChildAt(position);
    	
    	float centerX = view.getWidth()/2.0f;
    	float centerY = view.getHeight()/2.0f;
    	
    	Animation anim = new Rotate3dAnimation(0, 180, centerX, centerY, 100.0f, true);
		anim.setDuration(400l);
    	
		ViewHolder holder = (ViewHolder)view.getTag();
    	holder.city_name.setVisibility(View.GONE);
		holder.state_name.setVisibility(View.VISIBLE);
		mCitiId.add(holder.id);
		
		view.setAnimation(anim);
		view.startAnimation(anim);
		anim.setAnimationListener(new FlipAnimationListener());
	}

	private class FlipAnimationListener implements Animation.AnimationListener{

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			/*To start animation recursively*/
			pos += 1;
			startAnimation(pos);
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		
	} 
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.flipping, menu);
		return true;
	}

	private class MyArrayAdapter extends BaseAdapter{

        Context mContext;
        List<ListData> mListDataList;
        private LayoutInflater mInflater = null;
       
        public MyArrayAdapter(Context mContext, ArrayList<ListData> data){
           
            this.mContext = mContext;
            this.mListDataList = data;
            mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
       
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mListDataList != null ? mListDataList.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }
       
        public void setData(List<ListData> data) {
            mListDataList = data;
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.city_list_item, null);
                holder.city_name = (TextView)convertView.findViewById(R.id.city_name);
                holder.state_name = (TextView)convertView.findViewById(R.id.state_name);
                holder.delete_btn = (ImageButton)convertView.findViewById(R.id.delete_icon);
                convertView.setTag(holder);
               
            }else {
                holder = (ViewHolder)convertView.getTag();
            } 
            
            ListData data = mListDataList.get(position);
            
            if(mCitiId.contains(data.id)){
            	holder.city_name.setVisibility(View.GONE);
            	holder.state_name.setVisibility(View.VISIBLE);
            }else{
            	holder.city_name.setVisibility(View.VISIBLE);
            	holder.state_name.setVisibility(View.GONE);
            } 
            
            holder.city_name.setText(data.city);
            holder.state_name.setText(data.state);
            holder.id = data.id;
            
            final int posi = position;
            
            holder.delete_btn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DeleteItem(posi);
					
				}
			});
           
            return convertView;
        }
       
        public void DeleteItem(int position){
            Log.d(TAG, "DeleteItem(): before mListDataList.size() = "+mListDataList.size()+"position : "+position);
           
            ListData data = mListDataList.remove(position);
            if(mCitiId != null && mCitiId.size() > 0){
            	mCitiId.remove(new Integer(data.id));
            	pos--;
            }
            	 
            mCityList.remove(data);
            
            Log.d(TAG, "DeleteItem(): after mListDataList.size() = "+mListDataList.size());
            notifyDataSetChanged();
        }
       
    }
	
	 class ViewHolder{

         TextView city_name;
         TextView state_name;
         ImageButton delete_btn;
         int id;

     }
	
	public static class parsingListLoader extends AsyncTaskLoader<List<ListData>>{

        List<ListData> mListDataList;
        ProgressDialog progressDialog = null;
       Context context;
        public parsingListLoader(Context context) {
            super(context);
            this.context = context;
            progressDialog = new ProgressDialog(context);
        }

        @Override
        public List<ListData> loadInBackground() {
        	
            Log.d(TAG, "loadInBackground():");
            
            ListInfo listinfo = new ListInfo();
            FlippingDbAdapter dbAdapter = FlippingDbAdapter.getInstance(context);
            ListData cityArray[] = loadData();
            if(dbFlag){
            	dbAdapter.insertAllCitynStateList(cityArray);
            	dbFlag = false;
            }
            
            Cursor listCursor = dbAdapter.getCitynStateList();
            while(listCursor.moveToNext()){
            	
            	int id = listCursor.getInt(0);
            	String state = listCursor.getString(1);
            	String city = listCursor.getString(2);
            	
            	
            	listinfo.setData(city, state, id);
            	
            }
            
            return listinfo.getData();
        }
       
        /**
         * Called when there is new data to deliver to the client.  The
         * super class will take care of delivering it; the implementation
         * here just adds a little more logic.
         */
        @Override
        public void deliverResult(List<ListData> data) {

            Log.d(TAG, "deliverResult() called. data="+data);

            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            if (isReset()) {
                if (data != null) {
                    onReleaseResources(data);
                }
            }

            if (isStarted()) {
                super.deliverResult(data);
            }
        }

        /**
         * Handles a request to start the Loader.
         */
        @Override
        protected void onStartLoading() {
            Log.d(TAG, "onStartLoading() called.");

            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
            progressDialog.show();
           
            if (mListDataList != null) {
                deliverResult(mListDataList);
            }

            if (takeContentChanged() || mListDataList == null) {
                forceLoad();
            }

        }

        /**
         * Handles a request to stop the Loader.
         */
        @Override
        protected void onStopLoading() {
            Log.d(TAG, "onStopLoading() called.");
            // Attempt to cancel the current load task if possible.
            cancelLoad();
        }

        /**
         * Handles a request to cancel a load.
         */
        @Override
        public void onCanceled(List<ListData> data) {
            super.onCanceled(data);
            Log.d(TAG, "onCanceled() called.");

            // At this point we can release the resources associated with 'data'
            // if needed.
            onReleaseResources(data);
        }

        /**
         * Handles a request to completely reset the Loader.
         */
        @Override
        protected void onReset() {
            super.onReset();
            Log.d(TAG, "onReset() called.");

            onStopLoading();
            mListDataList = null;

        }

        /**
         * Helper function to take care of releasing resources associated
         * with an actively loaded data set.
         */
        protected void onReleaseResources(List<ListData> data) {
            Log.d(TAG, "onReleaseResources() called. data="+data);
            // For a simple List<> there is nothing to do.  For something
            // like a Cursor, we would close it here.
        }
       
    }
	
	@Override
	public Loader<List<ListData>> onCreateLoader(int id, Bundle args) {
		// TODO Auto-generated method stub
		return new parsingListLoader(aContext);
	}

	@Override
	public void onLoadFinished(Loader<List<ListData>> loader,
			List<ListData> data) {
		if(mtempList != null && mtempList.size() == 0){
    		mtempList.addAll(data);
    		Log.d(TAG, "onLoadFinished(): mtempList.size() = "+mtempList.size());
    	}
        mParseAdapter.setData(mCityList);
		
	}

	@Override
	public void onLoaderReset(Loader<List<ListData>> loader) {
		mParseAdapter.setData(null);
		
	}

	private static ListData[] loadData() {

		ListData cityArray[] = new ListData[13];
		int i = 0;

		ListInfo lInfo = new ListInfo();
		cityArray[0] = lInfo.new ListData("Bangalore", "Karnataka", i++);
		cityArray[1] = lInfo.new ListData("Kolkata", "WestBengal", i++);
		cityArray[2] = lInfo.new ListData("Mumbai", "Maharastra", i++);
		cityArray[3] = lInfo.new ListData("Lucknow", "UttarPradesh", i++);
		cityArray[4] = lInfo.new ListData("Bhopal", "MadhyaPradesh", i++);
		cityArray[5] = lInfo.new ListData("Patna", "Bihar", i++);
		cityArray[6] = lInfo.new ListData("Chandigadh", "Punjab", i++);
		cityArray[7] = lInfo.new ListData("Chennai", "Tamilnadu", i++);
		cityArray[8] = lInfo.new ListData("Guwahati", "Asam", i++);
		cityArray[9] = lInfo.new ListData("Gaya", "Bihar", i++);
		cityArray[10] = lInfo.new ListData("Ranchi", "Jharkhand", i++);
		cityArray[11] = lInfo.new ListData("Jabalpur", "MadhyaPradesh", i++);
		cityArray[12] = lInfo.new ListData("Hyderbad", "Telangana", i++);
		
		return cityArray;
	}
}
