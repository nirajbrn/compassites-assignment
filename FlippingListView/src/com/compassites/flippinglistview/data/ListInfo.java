package com.compassites.flippinglistview.data;

import java.util.ArrayList;

public class ListInfo {

ArrayList<ListData> cityDatalist = new ArrayList<ListData>();
	
	public void setData(String city, String state, int id){
		cityDatalist.add(new ListData(city, state, id));
	}
	
	public ArrayList<ListData> getData(){
		return cityDatalist;
	}
	
	public class ListData{
		public int id;
		public String city;
		public String state;
		
		public ListData(String city, String state, int id){
			this.city = city;
			this.state = state;
			this.id = id;
		}
		
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof ListData)) {
			    return false;
			  }
			ListData delUser = (ListData) o;
			  return city.equals(delUser.city) && id == delUser.id;
		}
	}
	
}
