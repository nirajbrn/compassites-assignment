
package com.compassites.flippinglistview.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.compassites.flippinglistview.data.ListInfo;
import com.compassites.flippinglistview.data.ListInfo.ListData;


public class FlippingDbAdapter {

	private static final String TAG = FlippingDbAdapter.class.getSimpleName();

	private static FlippingDbAdapter sInstance = null;
	
	private FlippingDbHelper mOpenHelper = null;

	private SQLiteDatabase mdb = null;
	
	private static Context mContext;

	private FlippingDbAdapter(Context context) {

		mContext = context;
		
		mOpenHelper = FlippingDbHelper.getInstance(mContext);
		mdb = mOpenHelper.getWritableDatabase();
	}

	public static FlippingDbAdapter getInstance(Context context){

		if(sInstance == null){
			sInstance = new FlippingDbAdapter(context);
		}

		return sInstance;
	}

	public Cursor getCitynStateList(){

		Log.d(TAG, "getCitynStateList():");
		

		Cursor listCursor = mdb.query(FlippingDb.CITY_LIST_TABLE_NAME, null, null, null, null, null, null);
		
		if (listCursor != null) {
			Log.d(TAG, "getCitynStateList(): cursor count="+listCursor.getCount());
		}
		
		return listCursor;
	}

	public void insertAllCitynStateList(ListData cityArray[]){

		
		Log.d(TAG, "insertAllCitynStateList() called cityArray="+cityArray);
		
		if (cityArray != null && cityArray.length > 0) {
		
			int len = cityArray.length;
			
			
			for (int i = 0; i < len; i++) {
				
				ContentValues cityListValues = new ContentValues();
				
				String cityName = cityArray[i].city; 
				String stateName = cityArray[i].state;

				cityListValues.put(FlippingDb.CityList.CITY_NAME, cityName); 
				cityListValues.put(FlippingDb.CityList.STATE_NAME, stateName);
				mdb.insert(FlippingDb.CITY_LIST_TABLE_NAME, null, cityListValues);
				
			}
			
		}
		
	}
	
	private static ListData[] loadData() {

		ListData cityArray[] = new ListData[9];
		int i = 0;

		ListInfo lInfo = new ListInfo();
		cityArray[0] = lInfo.new ListData("Bangalore", "Karnataka", i++);
		cityArray[1] = lInfo.new ListData("Kolkata", "WestBengal", i++);
		cityArray[2] = lInfo.new ListData("Mumbai", "Maharastra", i++);
		cityArray[3] = lInfo.new ListData("Lucknow", "UttarPradesh", i++);
		cityArray[4] = lInfo.new ListData("Bhopal", "MadhyaPradesh", i++);
		cityArray[5] = lInfo.new ListData("Patna", "Bihar", i++);
		cityArray[6] = lInfo.new ListData("Chandigadh", "Punjab", i++);
		cityArray[7] = lInfo.new ListData("Chennai", "Tamilnadu", i++);
		cityArray[9] = lInfo.new ListData("Gaya", "Bihar", i++);
		cityArray[10] = lInfo.new ListData("Ranchi", "Jharkhand", i++);
		cityArray[11] = lInfo.new ListData("Jabalpur", "MadhyaPradesh", i++);
		cityArray[12] = lInfo.new ListData("Hyderbad", "Telangana", i++);
		cityArray[13] = lInfo.new ListData("Vizag", "Andhrapradesh", i++);
		cityArray[14] = lInfo.new ListData("Ludhiyana", "Punjab", i++);
		cityArray[15] = lInfo.new ListData("Dehradun", "Uttaranchal", i++);
		
		return cityArray;
	}
}
